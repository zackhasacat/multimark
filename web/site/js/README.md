# modweb-js

This project provides javascript that can be used to put file links onto a mod website.

Follow the usage steps below to have download links that always point to your latest file, with no need to ever update them.

## Usage

For a Gitlab URL like `https://gitlab.com/my-cool-org/my-cool-repo`:

1. Add an element to your page that provides the Gitlab org of your mod's repo:

        <div id="modOrg" data-mod-org="my-cool-org"></div>

1. Add an element to your page that provides the name of your mod's repo:

        <div id="modProject" data-mod-project="my-cool-repo"></div>

1. Add an element to your page that will become the "Download" button for your mod:

        <a id="download">Download</a>

1. Optional: Add elements for shasums for your download (if you generate these):

        <a id="sha256">sha256</a>
        <a id="sha512">sha512</a>

1. Optional: Add an element to show errors that might happen when talking to Gitlab:

        <div id="errors"></div>

1. Add this code to your mod's repo:

        git submodule add https://gitlab.com/modding-openmw/modweb-js web/js

1. Finally, load this javascript on your page (put this just before the `</body>` tag):

        <script src="/js/gitlab.js"></script>
